# Bureaucrat

[![Build Status](https://drone.io/github.com/justinabrahms/bureaucrat/status.png)](https://drone.io/github.com/justinabrahms/bureaucrat/latest)

Bureaucrat is a library to help with client cert authentication. It is
a pure-go approach to the techniques described in [this
article](http://www.scriptjunkie.us/2013/11/adding-easy-ssl-client-authentication-to-any-webapp/)
which shells out to `openssl`.

Note: Within a month or so of me writing this, major browsers disabled support for certificate based authentication. So it goes.
